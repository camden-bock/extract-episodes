from utils import utils2

parentDirectory = '/home/imrelab/Nextcloud/A-Painter/'

utils2.make_episodes(workingDirectory=parentDirectory, csvPath="painting_episodes.csv",
                     outputDirectory='painting_episodes', outputAll=True)
utils2.makeRevealSlides(workingDirectory=parentDirectory, csvPath="painting_episodes.csv",
                        outputDirectory='painting_episodes')