from utils import figure_utils

parent_dir = '/home/imrelab/Nextcloud/A-Painter/painting_episodes/'

# fig 2 - drawing while teleporting, group 1
figure_utils.generate_panel(video_path='/home/imrelab/Nextcloud/A-Painter/InterviewComposite/Group1_part1_hq.mp4',
                            out_path=parent_dir + '/pmena-figures/fig2.png',
                            views=['p2', 'p2'],
                            start_times=['00:09:43.5', '00:09:51.00'])

# fig 1, 3-7
figure_utils.generate_panel_batch(video_parent_dir=parent_dir, csv_path='example_figures.csv', out_path=parent_dir + 'pmena-figures/')

