from utils import utils

srt_file = 'group2_part1.corrected.utf8.srt'
md_file = 'group2_part1.corrected.md'
csv_file = 'group2_part1.csv'
video_file = 'group2_part1.mp4'

utils.parseTranscript(srt_file, video_file, md_file, csv_file, gif=True)