import os
import pysrt
from tqdm import tqdm
import pandas as pd
import re
import datetime
import ffmpeg  # ffmpeg-python
import tabulate
import time


def parseTranscript(srtPath: str, videoPath: str, mdPath: str, csvPath: str, startTime: datetime = None,
                    endTime: datetime = None, gif: bool = False, png: bool = False,
                    markdownAnimations: bool = False) -> None:
    """
    Parases a SRT transcript
    :param srtPath: Path of SRT File
    :param videoPath: Path of Video File (mkv/mp4)
    :param mdPath:  Path of Markdown Ouput
    :param csvPath: Path of CSV output
    :param startTime: Start of Episode (optional)
    :param endTime: End of Episode (optinoal)
    :param gif: Output GIFs for Transcript?
    :param png: Output PNGs for Transcript?
    :param markdownAnimations:  Embed PNG or GIF in Markdown?
    """
    transcript = pysrt.open(srtPath, encoding='utf-8')
    # if (startTime != None) && (endTime != None):
    ## TODO verify this slice map from datetime
    # transcript = transcript.slice(starts_after=startTime, ends_before=endTime)

    os.makedirs('transcript/png/composite', exist_ok=True)
    os.makedirs('transcript/png/participant_left', exist_ok=True)
    os.makedirs('transcript/png/participant_right', exist_ok=True)
    os.makedirs('transcript/png/quest', exist_ok=True)
    os.makedirs('transcript/png/lab', exist_ok=True)
    os.makedirs('transcript/gif/composite', exist_ok=True)
    os.makedirs('transcript/gif/participant_left', exist_ok=True)
    os.makedirs('transcript/gif/participant_right', exist_ok=True)
    os.makedirs('transcript/gif/quest', exist_ok=True)
    os.makedirs('transcript/gif/lab', exist_ok=True)

    df = srt_toDataFrame(transcript, markdownAnimations)

    if png:
        make_graphics(df, videoPath, animation=False)

    if gif:
        make_graphics(df, videoPath, animation=True)

    write_csv(df.copy(), csvPath)
    write_markdown(df.copy(), mdPath)


def srt_toDataFrame(transcript: pysrt.srtfile, animation: bool = False) -> pd.DataFrame:
    """
    Create a Pandas DataFrame from an SRT File
    :param transcript: SRT Transcript from PYSRT
    :param animation: Embed gifs or pngs?
    :return: DataFrame with Speaker, Speech, Start, End and Path to Grpahics
    """
    transcript_df = pd.DataFrame(columns=['Start', 'End', 'Speaker', 'Speech'])

    for i in tqdm(range(0, len(transcript))):
        df_part = pd.DataFrame({'Start': subriptime_toTime(transcript[i].start),
                                'End': subriptime_toTime(transcript[i].end),
                                'Speaker': find_speaker(transcript[i].text),
                                'Speech': find_speech(transcript[i].text),
                                'Participant_Right': gif_reference(i, 'participant_right'),
                                'Participant_Left': gif_reference(i, 'participant_left'),
                                'Immersed_Interviewer': gif_reference(i, 'quest'),
                                'Room': gif_reference(i, 'lab'),
                                }, index=[1])
        transcript_df = pd.concat([transcript_df, df_part], ignore_index=True)
    return transcript_df


def find_speaker(transcript_text: str) -> str:
    """
    Finds speaker from syntax of SRT
    :param transcript_text: Input SRT text
    :return: Speaker Label
    """
    match = re.search('[A-z]\w*(?=:)', transcript_text)
    if match:
        return transcript_text[match.start():match.end()]
    return None


def find_speech(transcript_text: str) -> str:
    """
    Finds speech from structure of SRT text, removes speaker label
    :param transcript_text: SRT text
    :return: Speech from SRT
    """
    match = re.search('(?<=:\s)[\S\s]+', transcript_text)
    if match:
        return re.sub('\n', '', transcript_text[match.start():match.end()])
    return None


def png_reference(idx: int, view: str) -> str:
    """
    Local file refernece
    :param idx: index
    :param view: cropped view for column
    :return: local path as a markdown embedded image string
    """
    return '![' + view + ' frame](transcript/png/' + view + '/' + str(idx) + '.png)'


def gif_reference(idx, view):
    """
    Local file refernece
    :param idx: index
    :param view: cropped view for column
    :return: local path as a markdown embedded image string
    """
    return '![' + view + ' animation](transcript/gif/' + view + '/' + str(idx) + '.gif)'


def subriptime_toTime(srt: pysrt.srttime) -> datetime.datetime:
    """
    Reformats SRTTime to DateTime object
    :param srt: SubRippedTime
    :return: DateTime Object
    """
    time = datetime.time(hour=srt.hours, minute=srt.minutes, second=srt.seconds, microsecond=srt.milliseconds * 1000)
    return time


def make_graphics(df: pd.DataFrame, videoPath: str, animation: bool = False) -> None:
    """
    Iterates through a dataframe to generate graphics for each row
    :param df: DataFrame from Transcript
    :param videoPath: Path to Video for FFMPEG
    :param animation: PNG or GIF?
    """
    for i in tqdm(range(1, len(df.index))):
        if animation:
            make_gif(i, df.loc[i, 'Start'], df.loc[i, 'End'], videoPath)
        else:
            make_png(i, df.loc[i, 'Start'], df.loc[i, 'End'], videoPath)


def make_png(i: int, t_start, t_end, videoPath: str) -> None:
    """
    Generates a PNG for a row of the transcript dataframe
    :param i: index
    :param t_start: beginning time of line
    :param t_end: end time of line
    :param videoPath: path to video
    """
    input = ffmpeg.input(videoPath, ss=t_start, to=t_end)
    video = input.video

    outputs = [
        ffmpeg.output(video.filter('scale', size='hd1080'),
                      'transcript/png/composite/' + str(i) + '.png', vframes=1),
        ffmpeg.output(video.filter('crop', w=1920, h=1080, x=0, y=0),
                      'transcript/png/participant_left/' + str(i) + '.png', vframes=1),
        ffmpeg.output(video.filter('crop', w=1920, h=1080, x=1920, y=0),
                      'transcript/png/participant_right/' + str(i) + '.png', vframes=1),
        ffmpeg.output(video.filter('crop', w=1920, h=1080, x=0, y=1080),
                      'transcript/png/quest/' + str(i) + '.png', vframes=1),
        ffmpeg.output(video.filter('crop', w=1920, h=1080, x=1920, y=1080),
                      'transcript/png/lab/' + str(i) + '.png', vframes=1),
    ]
    for out in outputs:
        ffmpeg.run(out, quiet=True, overwrite_output=True)


def make_gif(i: int, t_start, t_end, videoPath: str) -> None:
    """
    Generates a GIF for a row of the transcript dataframe
    :param i: index
    :param t_start: beginning time of line
    :param t_end: end time of line
    :param videoPath: path to video
    """
    input = ffmpeg.input(videoPath, ss=t_start, to=t_end)
    split = (
        input.video
        .filter('scale',720, -1, flags='lanczos')
        .filter('fps', fps=12)
        .split()
    )
    palette = (
        split[0]
        .filter('palettegen')
    )

    video2 = ffmpeg.filter([split[1], palette], 'paletteuse')

    ## TODO add filter_complex pallet generation??
    outputs = [
        ffmpeg.output(video2,
                      'transcript/gif/composite/' + str(i) + '.gif'),
        ffmpeg.output(video2
                      .filter('crop', w=360, h=200, x=0, y=15),
                      'transcript/gif/participant_left/' + str(i) + '.gif'),
        ffmpeg.output(video2
                      .filter('crop', w=360, h=200, x=360, y=15),
                      'transcript/gif/participant_right/' + str(i) + '.gif'),
        ffmpeg.output(video2
                      .filter('crop', w=226, h=193, x=66, y=217),
                      'transcript/gif/quest/' + str(i) + '.gif'),
        ffmpeg.output(video2
                      .filter('crop', w=360, h=193, x=322, y=217),
                      'transcript/gif/lab/' + str(i) + '.gif'),
    ]
    for out in outputs:
        ffmpeg.run(out, quiet=True, overwrite_output=True)


def write_markdown(df: pd.DataFrame, path: str) -> None:
    """
    Writes a Markdown File with metadata and a table representation of the transcript
    :param df: DataFrame
    :param path: Path to Output
    """
    # NOTE KEEP
    # Before writing, cast Start and End to strformat
    df['Start'] = df['Start'].apply(lambda x: x.strftime('%M:%S.%f'))
    df['End'] = df['End'].apply(lambda x: x.strftime('%M:%S.%f'))

    file = open(path, 'w')
    file.write('# Transcription with Speaker Identification' '\n' + '\n')
    file.write('SRT File: ' + path + '\n' + '\n')
    file.write('Generated: ' + str(datetime.datetime.now().strftime('%a %b %d %H:%M:%S %Y')) + '\n' + '\n')
    file.write('Modified: ' + str(time.ctime(os.path.getmtime(path))) + '\n' + '\n')
    file.write(df.to_markdown())
    file.close()


def make_episodes(workingDirectory: str, videoPath: str, csvPath: str) -> None:
    """
    Use FFMPEG to break episodes into parts
    :param workingDirectory: location of CSV and video
    :param videoPath: Video input
    :param csvPath: CSV specifying episodes' names, start and end times
    """
    os.chdir(workingDirectory)

    os.makedirs('episodes/composite', exist_ok=True)
    os.makedirs('episodes/participant_left', exist_ok=True)
    os.makedirs('episodes/participant_right', exist_ok=True)
    os.makedirs('episodes/quest', exist_ok=True)
    os.makedirs('episodes/lab', exist_ok=True)
    os.makedirs("episodes/png", exist_ok=True)

    csvFile = pd.read_csv(csvPath)

    with tqdm(total=csvFile.shape[0] * 6, desc=workingDirectory, colour="green") as pbar:
        for row in csvFile.iterrows():
            episodeName = row[1].EpisodeName
            episodeName = episodeName.replace(" ", "")  # dropwhitespace

            pbar.desc = workingDirectory + ' : ' + episodeName

            input = ffmpeg.input(videoPath, ss=row[1].Start, to=row[1].End)
            video = input.video
            audio = input.audio

            outputs = [
                ffmpeg.output(video.filter('scale', size='hd1080'), audio,
                              'episodes/composite/' + episodeName + '.mp4', acodec='copy', vcodec='h264'),
                ffmpeg.output(video.filter('crop', w=1920, h=1080, x=0, y=0), audio,
                              'episodes/participant_left/' + episodeName + '.mp4', acodec='copy', vcodec='h264'),
                ffmpeg.output(video.filter('crop', w=1920, h=1080, x=1920, y=0), audio,
                              'episodes/participant_right/' + episodeName + '.mp4', acodec='copy', vcodec='h264'),
                ffmpeg.output(video.filter('crop', w=1920, h=1080, x=0, y=1080), audio,
                              'episodes/quest/' + episodeName + '.mp4', acodec='copy', vcodec='h264'),
                ffmpeg.output(video.filter('crop', w=1920, h=1080, x=1920, y=1080, size='hd1080'), audio,
                              'episodes/lab/' + episodeName + '.mp4', acodec='copy', vcodec='h264'),
                ffmpeg.output(video, audio,
                              'episodes/png/' + episodeName + '.png', vframes=1, acodec='copy', vcodec='h264')
            ]

            for out in outputs:
                ffmpeg.run(out, quiet=True, overwrite_output=True)
                pbar.update(1)
        pbar.close()


def write_csv(df: pd.DataFrame, path: str) -> None:
    """
    Writes a CSV copy of the Transcript
    :param df: DataFrame of the Transcript
    :param path: Path to output
    """
    df['Start'] = df['Start'].apply(lambda x: x.strftime('%M:%S.%f'))
    df['End'] = df['End'].apply(lambda x: x.strftime('%M:%S.%f'))

    df.loc[:, ['Start', 'End', 'Speaker', 'Speech']].to_csv(path, index=False)
