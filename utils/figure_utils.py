import os
import shutil
import subprocess
from datetime import datetime
import numpy
import ffmpeg
import pandas as pd
from wand.image import Image
from wand.font import Font
from wand.drawing import Drawing
from wand.color import Color
from wand.display import display


def generate_frame(video_path: str, view: str, frame: str, start_time: str = None, extra_crops: int = 0) -> Image:
    # grab frame from ffmpeg, make png
    frame_name = 'tmp_frames/' + str(frame) + '.png'

    video_input = ffmpeg.input(video_path, ss=start_time)
    output = ffmpeg.output(video_input.video, frame_name, vframes=1)
    ffmpeg.run(output, quiet=True, overwrite_output=True)

    # crop frame
    left_offset = 0
    top_offset = 0
    width = 1
    height = 1
    label = 'Composite'
    gravity = 'center'
    match view:
        case "p1":
            gravity = 'north_west'
            width = 0.5
            height = 0.5
            label = 'Participant 1'
        case "p2":
            gravity = 'north_east'
            width = 0.5
            height = 0.5
            label = 'Participant 2'
        case "interviewer":
            gravity = 'south_west'
            left_offset = .25
            width = 0.5
            height = 0.5
            label = 'Interviewer'
        case "classroom":
            gravity = 'south_east'
            width = 0.5
            height = 0.5
            label = 'Classroom'

    if frame == '0':
        label = 'View: ' + label

    style = Font(path='/usr/share/fonts/opentype/fira/firasanscompressed-regular.otf',
                 size=100, color='lightgrey')
    img = Image(filename=frame_name)
    img.crop(gravity=gravity, width=int(width * img.width), height=int(height * img.height))
    if left_offset != 0:
        img.crop(gravity='center', width=int((1 - 2 * left_offset) * img.width), height=int(img.height))
    if extra_crops != 0:
        extra_gravity = 'east'
        if extra_crops < 0:
            extra_gravity = 'west'
            extra_crops = abs(extra_crops)
        img.crop(gravity=extra_gravity, width=int((1 - extra_crops) * img.width), height=int(img.height))
    img.label(text=label, gravity='north_west', font=style)
    img.border('#79bde8', 5, 5)
    # with Drawing() as ctx:
    #     ctx.font_family = 'Fira Sans Compressed, Regular'
    #     ctx.font_size = 85
    #     ctx.fill_color = Color('White')
    #     ctx.stroke_color = Color('White')
    #     ctx.text(10, int(ctx.font_size)+10, label)
    #     ctx.gravity = "north_west"
    #     ctx.draw(img)
    img.format = 'png'
    return img


def generate_panel(video_path: str, out_path: str, views: numpy.array, start_times: numpy.array,
                   extra_crops: numpy.array = None) -> None:
    dim = str(len(views)) + 'x1'

    if extra_crops is None:
        extra_crops = [0] * len(views)

    # stash temporary files
    if not os.path.exists('tmp_frames'):
        os.mkdir('tmp_frames')

    with Image() as panel:
        for index, item in enumerate(views):
            panel.image_add(generate_frame(video_path=video_path, view=views[index], frame=str(index),
                                           start_time=start_times[index], extra_crops=extra_crops[index]))
        panel.montage(tile=dim, mode='concatenate')
        panel.format = 'png'
        panel.save(filename=out_path)

    # clean up
    if os.path.exists('tmp_frames'):
        shutil.rmtree('tmp_frames')


def generate_panel_batch(video_parent_dir: str, csv_path: str, out_path: str) -> None:
    csv_file = pd.read_csv(csv_path, header=0)
    figures = pd.unique(csv_file['figure'])
    for figure in figures:
        figure_frame_data = csv_file[csv_file['figure'] == figure].sort_values(by=['frame'])
        figure_meta_data = figure_frame_data[figure_frame_data['frame'] == 0]
        generate_panel(video_path=video_parent_dir + figure_meta_data['relative_path'].iat[0],
                       out_path=out_path + 'fig_' + f"{figure:0>3}" + '.png',
                       views=figure_frame_data['view'].to_numpy(),
                       start_times=figure_frame_data['start_time'].to_numpy(),
                       extra_crops=figure_frame_data['extra_crop'].to_numpy())
