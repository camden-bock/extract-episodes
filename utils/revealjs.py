from tqdm import tqdm

import pythonreveal
from pythonreveal import *
import pandas


def BuildPresentation(episodeMetadata: pandas.DataFrame, videoDirectory: str, exportPath: str) -> None:
    p = get_frontpage()
    group = 0;
    with tqdm(total=episodeMetadata.shape[0], colour="green") as pbar:
        for row in episodeMetadata.iterrows():
            episode_group = row[1].Group
            episode_name = str(row[1].Episode)
            episode_name = episode_name.replace(" ", "")
            episode_id = str(int(row[1].ID))

            if group != episode_group:
                group = episode_group
                get_section(p, group)

            get_episodePage(p, videoDirectory, episode_id, episode_name)
            pbar.update(1)
    p.export(exportPath + 'index.html')


def get_frontpage() -> pythonreveal.Presentation:
    p = Presentation(width=1920,
                     height=1080,
                     margin='0.05',
                     transition='none',
                     data_dir='./files/example/'
                     )
    page = p.frontpage()
    page.title('Episodes')
    author = page.author('IMRE Lab')
    author.institution('The University of Maine')
    return p


def get_section(p: pythonreveal.Presentation, group: int):
    section = p.sectionpage('Group ' + str(group))
    return section


def get_episodePage(p, videoDirectory: str, episode_id: int, episode_name: str):
    video_name = videoDirectory + '/webm/' + episode_id + '_' + episode_name + '.webm'

    page = p.page(episode_name + '  (#' + str(episode_id) + ')')
    div = page.div()
    div.addVideo(video_name)
    #div.add(get_episodeVideo(video_name))


def get_episodeVideo(video_name: str):
    return '<video controls> \n <source src="' + video_name + '"> \n </video>'
