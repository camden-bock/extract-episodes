import os

import ffmpeg
import pandas as pd
from tqdm import tqdm
import revealjs

def make_episodes(workingDirectory: str, csvPath: str, videoDirectory: str = 'InterviewComposite',
                  outputDirectory: str = 'episodes', outputAll: bool = True) -> None:
    """
    Use FFMPEG to break episodes into parts
    :param videoDirectory:
    :param workingDirectory: location of CSV and video
    :param csvPath: CSV specifying episodes' names, start and end times
    """
    os.chdir(workingDirectory)

    os.makedirs(outputDirectory + '/hq', exist_ok=True)
    if(outputAll):
        os.makedirs(outputDirectory + '/webm', exist_ok=True)
        os.makedirs(outputDirectory + '/gif', exist_ok=True)

    csv_file = pd.read_csv(csvPath, skiprows=[0])

    output_count = 3
    if not outputAll:
        output_count = 1
    with tqdm(total=csv_file.shape[0]*output_count, desc=workingDirectory, colour="green") as pbar:
        for row in csv_file.iterrows():
            episode_name = str(row[1].Episode)
            episode_name = episode_name.replace(" ", "")
            episode_name = str(int(row[1].ID)) + '_' + episode_name

            video_path = videoDirectory + '/Group' + str(row[1].Group) + '_part' + str(row[1].Part) + '_hq.mp4'

            pbar.desc = workingDirectory + ' : ' + episode_name

            video_input = ffmpeg.input(video_path, ss=row[1].Start, to=row[1].End)
            video = video_input.video
            audio = video_input.audio

            split = (
                video_input.video
                .filter('scale', 720, -1, flags='lanczos')
                .filter('fps', fps=12)
                .split()
            )
            palette = (
                split[0]
                .filter('palettegen')
            )
            preanimation_video = ffmpeg.filter([split[1], palette], 'paletteuse')

            outputs = [
                ffmpeg.output(video, audio,
                              outputDirectory + '/hq/' + episode_name + '.mp4',
                              acodec='copy', vcodec='libx264', crf=20, copyinkf=None),
                ffmpeg.output(video.filter('scale', 1080, -1, ), audio,
                              outputDirectory + '/webm/' + episode_name + '.webm',
                              format='webm', copyinkf=None),
                ffmpeg.output(preanimation_video,
                              outputDirectory + '/gif/' + episode_name + '.gif')
            ]

            if outputAll:
                for out in outputs:
                    ffmpeg.run(out, quiet=True, overwrite_output=True)
                    pbar.update(1)
            else:
                ffmpeg.run(outputs[0], quiet=True, overwrite_output=True)
                pbar.update(1)
        pbar.close()


def makeRevealSlides(workingDirectory: str, csvPath: str, videoDirectory: str = 'InterviewComposite',
                     outputDirectory: str = 'episodes'):
    os.chdir(workingDirectory)
    os.makedirs('slides/', exist_ok=True)

    csv_file = pd.read_csv(csvPath, skiprows=[0])
    outputDirectory = '../' + outputDirectory

    revealjs.BuildPresentation(csv_file, outputDirectory, 'slides/')

